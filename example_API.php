<?php

//example code:
//how to use the nem  API of public nodes in connection with the nodeexplorer.com API

// function => fetch all open API nodes with latest version from nodeexplorer.com
function get_nodes() {
    $server = "http://www.nodeexplorer.com/api_openapi_version";
    $data = file_get_contents($server);
    $obj = json_decode($data, true);
    $result = $obj['nodes'];
    return $result;
}

// function => fetch data from specific open API node
function get_node_data($server, $value) {
    $data = file_get_contents($server);
    $obj = json_decode($data);
    $result = $obj->{$value};
    return $result;
}

// function => skip to next node if data is empty, return "Unknown" if all open API nodes return no data
function check_node_data($request, $value) {
    $node = get_nodes();
    for ($i = 0; $i < count($node); $i++) {
        $result = get_node_data('http://' . $node[$i] . '/' . $request . '', $value);
        if (!empty($result)) {
            break;
        } else {
            $result = 'Unknown';
        }
    }
    return $result;
}

// output current blockchain height 
$blockchainHeight = check_node_data('chain/height', 'height');
echo("Current Blockchain Height #: $blockchainHeight <br />");



?>
